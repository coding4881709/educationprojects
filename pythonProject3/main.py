import requests
from datetime import date
from stock_functions import S_Functions
from News_class import news_tesla
from Mesage_maker import Message_Maker
from twilio.rest import Client
import os

STOCK = "TSLA"
# Sid = "AC8e52d4aca69955a9994e8b89d6617404"
# Code = "2C5SJE1GSC3PK8REAVR2SRPG"
COMPANY_NAME = "Tesla Inc"
api_for_stock = "IGZSUS53PHQSW8QF"
STOCK_ENDPOINT = "https://www.alphavantage.co/query"
NEWS_ENDPOINT = "https://newsapi.org/v2/everything"

account_sid = 'AC8e52d4aca69955a9994e8b89d6617404'
auth_token = 'b8115299bc151ca761cb88e77d0e948e'


#   !    !   !    !    !     !     !     !     Tests   !      !   !     !     !     ! !

# def b_e_l(num, sum):
#     total = num/sum
#     total = round(total, 4)
#     total = str(total) + "%"
#     return total
#
# def start(date=date):
#     r = requests.get("https://www.alphavantage.co/query?function=TIME_SERIES_DAILY&symbol=TSLA&apikey=IGZSUS53PHQSW8QF")
#     data = r.json()
#     data = data["Time Series (Daily)"]
#     current_time = date.today()
#     current_time = str(current_time)
#     date = current_time[9]
#     current_time = current_time.replace(date, str(int(date) - 1))
#     yesterday = data[current_time]["4. close"]
#     date = current_time[9]
#     current_time = current_time.replace(date, str(int(date) - 1))
#     be_yesterday = data[current_time]["4. close"]
#     total = b_e_l(float(yesterday) - float(be_yesterday), float(be_yesterday))
#     return total

## STEP 1: Use https://newsapi.org/docs/endpoints/everything
# When STOCK price increase/decreases by 5% between yesterday and the day before yesterday then print("Get News").
# HINT 1: Get the closing price for yesterday and the day before yesterday. Find the positive difference between the two prices. e.g. 40 - 20 = -20, but the positive difference is 20.
# HINT 2: Work out the value of 5% of yerstday's closing stock price.

#    !     !          !        !     Code      !            !           !         !

Stocks_Functions = S_Functions()
News_class = news_tesla()
Message_class = Message_Maker(Stocks_Functions.total_num)
if Stocks_Functions.check():
    Message_class.message_make(News_class.art1, News_class.art2, News_class.art3)
    client = Client(account_sid, auth_token)
    message = client.messages \
        .create(
        body=Message_class.message,
        from_='+12564747970',
        to='+48780095090'
    )
    print(message.sid)


## STEP 3: Use twilio.com/docs/sms/quickstart/python
# Send a separate message with each article's title and description to your phone number. 
# HINT 1: Consider using a List Comprehension.


# Optional: Format the SMS message like this:
"""
TSLA: 🔺2%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?. 
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
or
"TSLA: 🔻5%
Headline: Were Hedge Funds Right About Piling Into Tesla Inc. (TSLA)?. 
Brief: We at Insider Monkey have gone over 821 13F filings that hedge funds and prominent investors are required to file by the SEC The 13F filings show the funds' and investors' portfolio positions as of March 31st, near the height of the coronavirus market crash.
"""
