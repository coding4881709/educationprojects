class Message_Maker:

    def __init__(self, value):
        self.message = None
        if value < 0:
            self.value = str(value) + "%"
            self.value = self.value.replace("-", "🔻")
        else:
            self.value = "🔺" + str(value) + "%"

    def message_make(self, new1, new2, new3):
        self.message = f"""
        TSLA: {self.value}
        
        #############################
        Title: {new1["title"]}. (TSLA)?. 
        
        Brief: {new1["description"]}
        
        URL: {new1["url"]}
        ##############################
        
        Title: {new2["title"]}. (TSLA)?. 
        
        Brief: {new2["description"]}
        
        URL: {new2["url"]}
        ##############################
        
        Title: {new3["title"]}. (TSLA)?. 
        
        Brief: {new3["description"]}
        
        URL: {new3["url"]}
        ##############################
        """
