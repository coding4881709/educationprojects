from data_manager import DataManager
from flight_data import FlightData
from flight_search import FlightSearch
from datetime import datetime
from notification_manager import NotificationManager

notification_class = NotificationManager()
data_manager = DataManager()
sheet_data = data_manager.get_destination_data()
# print(sheet_data)\
flight_search = FlightSearch()

#  5. In main.py check if sheet_data contains any values for the "iataCode" key.
#  If not, then the IATA Codes column is empty in the Google Sheet.
#  In this case, pass each city name in sheet_data one-by-one
#  to the FlightSearch class to get the corresponding IATA code
#  for that city using the Flight Search API.
#  You should use the code you get back to update the sheet_data dictionary.
# if sheet_data[0]["iataCode"] == "":
#     from flight_search import FlightSearch
#
#     flight_search = FlightSearch()
#     for row in sheet_data:
#         row["iataCode"] = flight_search.get_destination_code(row["city"])
#     print(sheet_data)
#
#     data_manager.destination_data = sheet_data
#     data_manager.update_destination_codes()
#
for count, destini in enumerate(sheet_data):
    flight = flight_search.get_data(destini)
    if flight is None:
        continue
    else:
        flight.out_date = datetime.fromtimestamp(flight.out_date)
        if flight.price < data_manager.destination_data[count]['lowestPrice']:
            notification_class.send_message(flight.origin_city, flight.price, flight.destination_city,
                                            flight.out_date, flight.origin_airport, flight.destination_airport)
