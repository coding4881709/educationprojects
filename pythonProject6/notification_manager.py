class NotificationManager:

    def send_message(self, city, price, des_city, date, out_air, arr_air):
        print(f'Low price alert! Only ${price} '
              f'to fly from {city}-{out_air} to {des_city}-{arr_air}, on {date}')

    def send_message2(self, city, price, des_city, date, out_air, arr_air, stops, where):
        print(f'Low price alert! Only ${price} '
              f'to fly from {city}-{out_air} to {des_city}-{arr_air}, '
              f'on {date}\n\n {stops} Stop over, at {where}')



