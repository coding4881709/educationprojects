import requests
from datetime import datetime, timedelta
from flight_data import FlightData

TEQUILA_ENDPOINT = "https://tequila-api.kiwi.com"
TEQUILA_API_KEY = "garlwRmosvJOTRfxJP_U4JP2_8Bphy_5"

header = {
    "apikey": TEQUILA_API_KEY
}


class FlightSearch:
    def get_destination_code(self, city_name):
        location_endpoint = f"{TEQUILA_ENDPOINT}/locations/query"
        headers = {"apikey": TEQUILA_API_KEY}
        query = {"term": city_name, "location_types": "city"}
        response = requests.get(url=location_endpoint, headers=headers, params=query)
        results = response.json()["locations"]
        code = results[0]["code"]
        return code

    def get_data(self, json):
        tequila_url = f"{TEQUILA_ENDPOINT}/search"
        time_now = datetime.now()
        after_6_months = time_now + timedelta(days=180)

        time_now = f'{time_now.strftime("%d")}/{time_now.strftime("%m")}/{time_now.strftime("%Y")}'
        after_6_months = f'{after_6_months.strftime("%d")}/{after_6_months.strftime("%m")}/{after_6_months.strftime("%Y")}'

        data = {
            "fly_from": "GDN",
            "fly_to": json["iataCode"],
            "date_from": time_now,
            "date_to": after_6_months,
            'stop_overs': 0

        }

        response = requests.get(url=tequila_url, headers=header, params=data)
        try:
            data = response.json()["data"][0]
            flight_data = FlightData(
                price=data["price"],
                origin_city=data["cityFrom"],
                origin_airport=data["flyFrom"],
                destination_city=data["cityTo"],
                destination_airport=data["flyTo"],
                out_date=data["dTime"],
            )
            return flight_data
        except KeyError or NameError:
            return None
