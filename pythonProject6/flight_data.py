import requests
from datetime import datetime, timedelta

TEQUILA_API_KEY = "garlwRmosvJOTRfxJP_U4JP2_8Bphy_5"
TEQUILA_ENDPOINT = "https://tequila-api.kiwi.com"

header = {
    "apikey": TEQUILA_API_KEY
}


class FlightData:

    def __init__(self, price, origin_city, origin_airport, destination_city, destination_airport, out_date, stop_overs=0, via_city=""):
        self.price = price
        self.origin_city = origin_city
        self.origin_airport = origin_airport
        self.destination_city = destination_city
        self.destination_airport = destination_airport
        self.out_date = out_date

        self.stop_overs = stop_overs
        self.via_city = via_city
