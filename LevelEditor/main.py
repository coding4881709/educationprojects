import pygame
from button import Button


pygame.init()

clock = pygame.time.Clock()
FPS = 60

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 580
LOW_MARGIN = 100
SIDE_MARGIN = 300

screen = pygame.display.set_mode((SCREEN_WIDTH + SIDE_MARGIN, SCREEN_HEIGHT + LOW_MARGIN))
pygame.display.set_caption('Level Editor')

# Variables
scrolling_left = False
scrolling_right = False
scroll = 0
scroll_speed = 1
ROWS = 16
current_tile = 0
MAX_COLUMNS = 150
TILE_SIZE = SCREEN_HEIGHT // ROWS
TILE_TYPES = 20

# Data
world_data = []
for row in range(ROWS):
    r = [-1] * MAX_COLUMNS
    world_data.append(r)

# create ground
for tile in range(0, MAX_COLUMNS):
    world_data[ROWS - 1][tile] = 0

# Colors
Red = (200, 25, 25)
White = (255, 255, 255)
Green = (144, 201, 120)

# Load images
pine1_img = pygame.image.load('img/pine1.png')
pine2_img = pygame.image.load('img/pine2.png')
mountain_img = pygame.image.load('img/mountain.png')
sky_img = pygame.image.load('img/sky_cloud.png')
img_list = []
for x in range(TILE_TYPES):
    img = pygame.image.load(f'img/other/{x}.png')
    img = pygame.transform.scale(img, (TILE_SIZE, TILE_TYPES))
    img_list.append(img)


def draw_bg():
    screen.fill(Green)
    width = sky_img.get_width()
    for x in range(4):
        screen.blit(sky_img, ((x * width) - scroll * 0.5, 0))
        screen.blit(mountain_img, ((x * width) - scroll * 0.6, SCREEN_HEIGHT - mountain_img.get_height() - 300))
        screen.blit(pine1_img, ((x * width) - scroll * 0.7, SCREEN_HEIGHT - pine1_img.get_height() - 150))
        screen.blit(pine2_img, ((x * width) - scroll * 0.8, SCREEN_HEIGHT - pine2_img.get_height()))


def draw_grid():
    # vertical lines
    for c in range(MAX_COLUMNS + 1):
        pygame.draw.line(screen, White, (c * TILE_SIZE - scroll, 0), (c * TILE_SIZE - scroll, SCREEN_HEIGHT))
    # horizontal lines
    for c in range(ROWS + 1):
        pygame.draw.line(screen, White, (0, c * TILE_SIZE), (SCREEN_WIDTH, c * TILE_SIZE))


def draw_world():
    for y, row in enumerate(world_data):
        for x, tile in enumerate(row):
            if tile >= 0:
                screen.blit(img_list[tile], (x * TILE_SIZE - scroll, y * TILE_SIZE))


# create buttons
buttons_list = []
button_row = 0
button_col = 0
for i in range(len(img_list)):
    button = Button(SCREEN_WIDTH + (75 * button_col) + 50, 75 * button_row + 50, img_list[i], 1)
    buttons_list.append(button)
    button_col += 1
    if button_col == 3:
        button_row += 1
        button_col = 0

run = True
while run:
    clock.tick(FPS)

    draw_bg()
    draw_grid()
    draw_world()

    pygame.draw.rect(screen, Green, (SCREEN_WIDTH, 0, SIDE_MARGIN, SCREEN_HEIGHT))

    button_count = 0
    for button_count, i in enumerate(buttons_list):
        if i.draw(screen):
            current_tile = button_count

    pygame.draw.rect(screen, Red, buttons_list[current_tile].rect, 3)

    if scrolling_left:
        if not scroll - (7 * scroll_speed) < 0:
            scroll -= 7 * scroll_speed
    if scrolling_right:
        scroll += 7 * scroll_speed

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                scrolling_left = True
            if event.key == pygame.K_RIGHT:
                scrolling_right = True
            if event.key == pygame.K_LSHIFT:
                scroll_speed = 5
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT:
                scrolling_left = False
            if event.key == pygame.K_RIGHT:
                scrolling_right = False
            if event.key == pygame.K_LSHIFT:
                scroll_speed = 1

    pygame.display.update()

pygame.quit()
