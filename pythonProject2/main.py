import requests

# "https://api.openweathermap.org/data/2.5/weather?lat=54.356030&lon=18.646120&appid"
#                         "=a5dc47900b93e9224f5baff5f636b44b"
# api_key = "a5dc47900b93e9224f5baff5f636b44b"
site = "https://api.openweathermap.org/data/2.5/onecall"
#
long = "18.646120"
lat = "54.356030"
data = {
    "lat": 54.356030,
    "lon": 18.646120,
    "appid": "a5dc47900b93e9224f5baff5f636b44b",
}

response = requests.get(site, params=data)
response.raise_for_status()
print(response.json())
