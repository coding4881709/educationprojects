import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from time import sleep

Url = 'https://docs.google.com/forms/d/e/1FAIpQLSct4aTFTxf8yAEQbQ1V8bwd_U_WTuxTUXq2sA85wzsmqeS-xw/viewform?usp=sf_link'

response = requests.get('https://appbrewery.github.io/Zillow-Clone/')
soup = BeautifulSoup(response.text, "html.parser")

prices = soup.find_all(class_='PropertyCardWrapper__StyledPriceLine')
full_prices = []
for item in prices:
    full_prices.append(item.text.replace('+/mo', '').replace('/mo', '').replace('+ 1 bd',
                                                                                '').replace('+ 1ld', '').replace(
        '+ 1bd', ''))

links = soup.find_all(class_='property-card-link')
full_links = []
for link in links:
    full_links.append(link['href'])

addresses = soup.find_all(name='address')
full_addresses = []
for address in addresses:
    full_addresses.append((address.text.strip()).replace(',', '').replace('|', ''))
print(full_addresses)
chrome_options = Options()
chrome_options.add_experimental_option("detach", True)
driver = webdriver.Chrome(options=chrome_options)

for index in range(len(full_addresses)):
    driver.get(Url)
    driver.find_element(By.XPATH, '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[1]/div/div/div[2]/div/div[1]/div/div['
                                  '1]/input').send_keys(full_addresses[index - 1])
    driver.find_element(By.XPATH, '//*[@id="mG61Hd"]/div[2]/div/div[2]/div[2]/div/di'
                                  'v/div[2]/div/div[1]/div/div[1]/input').send_keys(full_prices[index - 1])
    driver.find_element(By.XPATH, '//*[@id="mG61Hd"]/div[2]/div/div[2]/'
                                  'div[3]/div/div/div[2]/div/div[1]/div/div[1]/input').send_keys(full_links[index - 1])
    driver.find_element(By.XPATH, '//*[@id="mG61Hd"]/div[2]/div/div[3]/div[1]/div[1]/div/span/span').click()

