from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
import threading
import re
import time

chrome_options = Options()
chrome_options.add_experimental_option("detach", True)

chrome_driver_path = '/.../gosza/Documents/chromedriver'
driver = webdriver.Chrome(options=chrome_options)

driver.get('https://orteil.dashnet.org/experiments/cookie/')

items = driver.find_elements(by=By.CSS_SELECTOR, value="#store div")
item_ids = [item.get_attribute("id") for item in items]
cookie = driver.find_element('id', 'cookie')
timeout = time.time() + 5
five_min = time.time() + 60*5  # 5 minutes

# def check_maximum(amount, upgrades: list):
#     maximum = []
#     if "," in amount:
#         amount = amount.replace(",", "")
#     amount = int(amount)
#     for item in upgrades:
#         if amount >= int((re.findall(r'\d+', item.text)[0])):
#             maximum.append(int((re.findall(r'\d+', item.text)[0])))
#     return maximum
#
#
# def checking():
#     threading.Timer(5.0, checking).start()
#     amount = driver.find_element('id', 'money')
#     upgrades = driver.find_elements(By.CSS_SELECTOR, 'div div div div b')
#     prices = check_maximum(int(amount.text), upgrades)
#     cookie_upgrades = {}
#     for n in range(len(upgrades)):
#         cookie_upgrades[prices[n]] = item_ids[n]



# checking()
while True:
    cookie.click()
    if time.time() > timeout:

        # Get all upgrade <b> tags
        all_prices = driver.find_elements(by=By.CSS_SELECTOR, value="#store b")
        item_prices = []

        # Convert <b> text into an integer price.
        for price in all_prices:
            element_text = price.text
            if element_text != "":
                cost = int(element_text.split("-")[1].strip().replace(",", ""))
                item_prices.append(cost)

        # Create dictionary of store items and prices
        cookie_upgrades = {}
        for n in range(len(item_prices)):
            cookie_upgrades[item_prices[n]] = item_ids[n]

        # Get current cookie count
        money_element = driver.find_element(by=By.ID, value="money").text
        if "," in money_element:
            money_element = money_element.replace(",", "")
        cookie_count = int(money_element)

        # Find upgrades that we can currently afford
        affordable_upgrades = {}
        for cost, id in cookie_upgrades.items():
            if cookie_count > cost:
                affordable_upgrades[cost] = id

        # Purchase the most expensive affordable upgrade
        highest_price_affordable_upgrade = max(affordable_upgrades)
        print(highest_price_affordable_upgrade)
        to_purchase_id = affordable_upgrades[highest_price_affordable_upgrade]

        driver.find_element(by=By.ID, value=to_purchase_id).click()

        # Add another 5 seconds until the next check
        timeout = time.time() + 5

    if time.time() > five_min:
        cookie_per_s = driver.find_element(by=By.ID, value="cps").text
        print(cookie_per_s)
        break

driver.quit()
driver.close()