import random
import pygame


pygame.init()
screen_width = 600
screen_height = 750

pygame.mixer.pre_init(44100, -16, 2, 512)
pygame.mixer.init()

screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Space Invaders')
clock = pygame.time.Clock()
#Sounds
explosion_fx =pygame.mixer.Sound('img/img_explosion.wav')
explosion_fx.set_volume(0.25)

explosion2_fx =pygame.mixer.Sound('img/explosion2.wav')
explosion2_fx.set_volume(0.25)

laser_fx =pygame.mixer.Sound('img/laser.wav')
laser_fx.set_volume(0.25)



# Variables
run = True
fps = 60
rows = 5
columns = 5
last_alien_shot = pygame.time.get_ticks()
alien_cooldown = 1000
red = (255, 0, 0)
green = (0, 255, 0)
white = (255, 255, 255)
countdown = 3
last_tick = pygame.time.get_ticks()
font30 = pygame.font.SysFont('Nova Square', 30)
font40 = pygame.font.SysFont('Nova Square', 40)
game_over = 0


# Images
bg = pygame.image.load('img/bg.jpg')


def draw_bg():
    screen.blit(bg, (0, 0))

def draw_text(text, font, color, x, y):
    img = font.render(text, True, color)
    screen.blit(img, (x, y))


class SpaceShip(pygame.sprite.Sprite):
    def __init__(self, x, y, health):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('img/spaceship.png')
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]
        self.health_start = health
        self.health_remaining = health
        self.last_shot = pygame.time.get_ticks()

    def update(self):
        speed = 8
        cooldown = 500

        key = pygame.key.get_pressed()
        if key[pygame.K_LEFT] and self.rect.left > 0:
            self.rect.x -= speed
        if key[pygame.K_RIGHT] and self.rect.right < screen.get_width():
            self.rect.x += speed


        self.mask = pygame.mask.from_surface(self.image)


        time_in_now = pygame.time.get_ticks()
        if key[pygame.K_SPACE] and time_in_now - self.last_shot > cooldown:
            laser_fx.play()
            bullet = Bullet(self.rect.centerx, self.rect.top)
            bullet_group.add(bullet)
            self.last_shot = pygame.time.get_ticks()

        pygame.draw.rect(screen, 'red', (self.rect.x, (self.rect.bottom + 10), self.rect.width, 15))
        if self.health_remaining > 0:
            pygame.draw.rect(screen, 'green', (self.rect.x, (self.rect.bottom + 10),
                                               int(self.rect.width * (self.health_remaining / self.health_start)), 15))
        elif self.health_remaining <= 0:
            global game_over
            explosion = Explosion(self.rect.centerx, self.rect.centery, 3)
            explosion_group.add(explosion)
            self.kill()
            game_over = -1

class Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('img/bullet.png')
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]

    def update(self):
        self.rect.y -= 5
        if self.rect.bottom < 0:
            self.kill()
        if pygame.sprite.spritecollide(self, aliens_group, True):
            self.kill()
            explosion_fx.play()
            explosion = Explosion(self.rect.centerx, self.rect.centery, 2)
            explosion_group.add(explosion)


class Aliens(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('img/alien' + str(random.randint(1, 5)) + '.png')
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]
        self.move_count = 0
        self.move_direction = 1

    def update(self):
        self.rect.x += self.move_direction
        self.move_count += 1
        if abs(self.move_count) > 75:
            self.move_direction *= -1
            self.move_count *= self.move_direction


class Alien_Bullet(pygame.sprite.Sprite):
    def __init__(self, x, y):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load('img/alien_bullet.png')
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]

    def update(self):
        self.rect.y += 2
        if self.rect.top > screen_height:
            self.kill()
        if pygame.sprite.spritecollide(self, space_ship_group, False, pygame.sprite.collide_mask):
            self.kill()
            explosion2_fx.play()
            space_ship.health_remaining -= 1
            explosion = Explosion(self.rect.centerx, self.rect.centery, 1)
            explosion_group.add(explosion)

class Explosion(pygame.sprite.Sprite):
    def __init__(self, x, y, size):
        self.images = []
        pygame.sprite.Sprite.__init__(self)
        for num in range(5):
            image = pygame.image.load(f'img/exp{num + 1}.png')
            if size == 1:
                image = pygame.transform.scale(image, (20, 20))
            if size == 2:
                image = pygame.transform.scale(image, (40, 40))
            if size == 3:
                image = pygame.transform.scale(image, (160, 160))
            self.images.append(image)
        self.index = 0
        self.image = self.images[self.index]
        self.rect = self.image.get_rect()
        self.rect.center = [x, y]
        self.counter = 0

    def update(self):
        explosion_speed = 5

        self.counter += 1
        if self.counter >= explosion_speed and self.index < len(self.images) -1:
            self.counter = 0
            self.index += 1
            self.image = self.images[self.index]
        if self.index >= len(self.images) -1 and self.counter >= explosion_speed:
            self.kill()


space_ship_group = pygame.sprite.Group()
bullet_group = pygame.sprite.Group()
aliens_group = pygame.sprite.Group()
aliens_bullet_group = pygame.sprite.Group()
explosion_group = pygame.sprite.Group()


def create_aliens():
    for row in range(rows):
        for item in range(columns):
            alien = Aliens(100 + item * 100, 100 + row * 70)
            aliens_group.add(alien)


space_ship = SpaceShip(int(screen_width / 2), screen_height - 100, 3)
space_ship_group.add(space_ship)
create_aliens()

while run:
    clock.tick(fps)
    # Functions
    draw_bg()
    if countdown == 0:
        time_now = pygame.time.get_ticks()
        if time_now - last_alien_shot > alien_cooldown and len(aliens_bullet_group) < 5 and len(aliens_group) > 0:
            attacking_alien = random.choice(aliens_group.sprites())
            alien_bullet = Alien_Bullet(attacking_alien.rect.centerx, attacking_alien.rect.bottom)
            aliens_bullet_group.add(alien_bullet)
            last_alien_shot = time_now

        if len(aliens_group) <= 0:
            game_over = 1
        if game_over == 0:
            bullet_group.update()
            aliens_group.update()
            space_ship.update()
            aliens_bullet_group.update()
        elif game_over == -1:
            draw_text('GAME OVER!', font40, white, int(screen_width / 2 - 85), int(screen_height / 2 + 50))
        elif game_over == 1:
            draw_text('YOU WIN!', font40, white, int(screen_width / 2 - 85), int(screen_height / 2 + 50))


    aliens_bullet_group.draw(screen)
    space_ship_group.draw(screen)
    bullet_group.draw(screen)
    aliens_group.draw(screen)
    explosion_group.update()
    explosion_group.draw(screen)


    if countdown > 0:
        draw_text('GET READY!', font40, white, int(screen_width / 2 - 85), int(screen_height / 2 + 50))
        draw_text(str(countdown), font40, white, int(screen_width / 2 - 10), int(screen_height / 2 + 100))
        count_ticks = pygame.time.get_ticks()
        if count_ticks - last_tick >= 1000:
            countdown -= 1
            last_tick = count_ticks




    # Events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

    pygame.display.update()

pygame.quit()
