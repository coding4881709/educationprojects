import requests
from datetime import datetime

header_key = "Basic a3JpZnRpazpNb2MxMWdtYWls"
user = "kriftik"
password = "Moc11gmail"

API_ID = "fdf89cd7"
API_KEY = "4745c4b68d5a201c102e88f1a2a3109a"
endpoint = "https://trackapi.nutritionix.com/v2/natural/exercise"
header = {
    "x-app-id": API_ID,
    "x-app-key": API_KEY,
}

answer = input("Tell me which exercises you did:")

json1 = {
    "query": answer
}

response = requests.post(url="https://trackapi.nutritionix.com/v2/natural/exercise", headers=header, json=json1)

response_json = response.json()

response_json = response_json["exercises"]

sheety_endpoint = "https://api.sheety.co/b2c29317840cb9f0ed97ec33811e1fe0/myWorkouts/workouts"
headers = {
    "Authorization": "Basic a3JpZnRpazpNb2MxMWdtYWls"
}

today = datetime.today()
today = f"{today.strftime('%Y')}/{today.strftime('%m')}/{today.strftime('%d')}"
time = (datetime.now()).strftime("%X")
for item in response_json:
    json2 = {
        "workout": {
            "Date": today,
            "Time": time,
            "Exercise": item["user_input"].capitalize(),
            "Duration": item["duration_min"],
            "Calories": item["nf_calories"]
        }
    }

    response2 = requests.post(url=sheety_endpoint, json=json2, headers=headers)
    print(response2)
