import requests
from bs4 import BeautifulSoup

header = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 '
                  'Safari/537.36',
    'Accept-Language': 'pl-PL,pl;q=0.9,ru-RU;q=0.8,ru;q=0.7,en-US;q=0.6,en;q=0.5',
}

response = requests.get('https://www.amazon.pl/eXtremeRate-Wielka-Housing-Faceplate-Controller/dp/B0B64Y7HRK/ref'
                        '=d_pd_day0_sccl_2_4/259-9748333-2358909?pd_rd_w=D3X1D&content-id=amzn1.sym.8313e262-df93'
                        '-4b3e-be3a-276f2f8182b4&pf_rd_p=8313e262-df93-4b3e-be3a-276f2f8182b4&pf_rd_r'
                        '=YFRSEXF3H3S537N7SJRR&pd_rd_wg=6ib0k&pd_rd_r=88ea6c29-64db-4bd7-b648-ba392a56c690&pd_rd_i'
                        '=B075JFHSK9&th=1')


soup = BeautifulSoup(response.text, 'html.parser')

price_whole = soup.find(name='span', class_='a-price-whole')
price_fraction = soup.find(name='span', class_='a-price-fraction')

full_price = int((price_whole.getText()).replace(",", "") + price_fraction.getText())

print(full_price)


