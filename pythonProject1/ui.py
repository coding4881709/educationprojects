from tkinter import *
from quiz_brain import QuizBrain
from time import sleep

THEME_COLOR = "#375362"


class QuizInterFace:

    def __init__(self, quiz_brain: QuizBrain):
        self.score = 0
        self.quiz = quiz_brain
        self.window = Tk()
        self.window.title("Quizzler")
        self.window.config(background=THEME_COLOR)
        self.label = Label(text="Score: 0", background=THEME_COLOR, fg="white", font=('Times New Roman', 15, 'bold'))
        self.label.grid(column=1, row=0)
        self.canvas = Canvas(width=300, height=250, bg="white")
        self.question_text = self.canvas.create_text(150, 125, text="BbBa", font=("Arial", 16, "italic"), width=280)
        self.canvas.grid(column=0, row=1, columnspan=2, pady=20, padx=20)
        photo = PhotoImage(file="images/true.png")
        self.right_button = Button(image=photo, borderwidth=0, command=self.check_true)
        self.right_button.grid(column=0, row=2, pady=(0, 30))
        photo2 = PhotoImage(file="images/false.png")
        self.false_button = Button(image=photo2, borderwidth=0, command=self.check_false)
        self.false_button.grid(column=1, row=2, pady=(0, 30))
        self.get_next_question()
        self.window.mainloop()

    def get_next_question(self):
        self.canvas.configure(bg="white")
        q_text = self.quiz.next_question()
        self.canvas.itemconfig(self.question_text, text=q_text)

    def check_false(self):
        if self.quiz.still_has_questions():
            is_true = self.quiz.check_answer("False")
            self.feedback(is_true)
        else:
            self.canvas.itemconfig(self.question_text, text="You reached the limit!")

    def check_true(self):
        if self.quiz.still_has_questions():
            is_true = self.quiz.check_answer("True")
            self.feedback(is_true)
        else:
            self.canvas.itemconfig(self.question_text, text="You reached the limit!")

    def feedback(self, is_true):
        if is_true:
            self.canvas.configure(bg="green")
            self.score += 1
            self.label.config(text=f"Score: {self.score}")
        else:
            self.canvas.configure(bg="red")
        self.window.after(500, self.get_next_question())
