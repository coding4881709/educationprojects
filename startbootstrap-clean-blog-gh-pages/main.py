from flask import Flask, render_template
import requests

app = Flask(__name__)

url = 'https://api.npoint.io/c4baf31ae044a1761774/'
json = requests.get(url).json()

post_titles = []
post_subtitles = []
post_bodys = []

for index in range(len(json)):
    post_subtitles.append(json[index]['subtitle'])
    post_titles.append(json[index]['title'])
    post_bodys.append(json[index]['body'])


@app.route('/')
def hello_world():
    return render_template('index.html', titles=post_titles, subtitles=post_subtitles)


@app.route('/about')
def about():
    return render_template('about.html')


@app.route('/contact')
def contact():
    return render_template('contact.html')


@app.route('/post/<int:id>')
def post(id):
    data = [post_titles[id], post_subtitles[id], post_bodys[id]]
    return render_template('post.html', data=data)


# @app.route('/<number>')
# def number():
#      pass

app.run(debug=True)
