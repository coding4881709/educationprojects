from flask import Flask, render_template
from post import Post

app = Flask(__name__)
post1 = Post(0)
post2 = Post(1)
post3 = Post(2)

posts_title = [post1.title, post2.title, post3.title]

posts_subtitle = [post1.subtitle, post2.subtitle, post3.subtitle]

posts_body = [post1.body, post2.body, post3.body]


@app.route('/')
def home():

    return render_template("index.html", title=posts_title, subtitle=posts_subtitle, body=posts_body)
@app.route("/post/<int:index>")
def post(index):
    return render_template("post.html", title=posts_title[index], subtitle=posts_subtitle[index], body=posts_body[index])
if __name__ == "__main__":
    app.run(debug=True)
