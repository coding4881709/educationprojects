import pygame
from sys import exit

# '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'

pygame.init()
screen = pygame.display.set_mode((600, 500))
pygame.display.set_caption('MonsterTale')
icon = pygame.image.load('images/Untitled.png')
pygame.display.set_icon(icon)
clock = pygame.time.Clock()

# Colors
bg = (50, 25, 50)
white = (255, 255, 255)

# Font
font = pygame.font.SysFont('Nova Square', 30)

# Variables
live_ball = False
margin = 50
cpu_score = 0
player_score = 0
winner = 0
speed_increase = 0


def draw_text(text, text_font, text_color, x, y):
    img = font.render(text, True, text_color)
    screen.blit(img, (x, y))


def draw_board():
    screen.fill(bg)
    pygame.draw.line(screen, white, (0, margin), (screen.get_width(), margin))


class paddle:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.rect = pygame.Rect(self.x, self.y, 20, 100)
        self.speed = 5

    def move(self):
        key = pygame.key.get_pressed()
        if key[pygame.K_UP] and self.rect.top > margin:
            self.rect.move_ip(0, -1 * self.speed)
        if key[pygame.K_DOWN] and self.rect.bottom < screen.get_height():
            self.rect.move_ip(0, self.speed)

    def ai(self):
        if self.rect.centery < ball.rect.top and self.rect.bottom < screen.get_height():
            self.rect.move_ip(0, self.speed)
        if self.rect.centery > ball.rect.bottom and self.rect.top > margin:
            self.rect.move_ip(0, -1 * self.speed)

    def draw(self):
        pygame.draw.rect(screen, white, self.rect)


class ball:
    def __init__(self, x, y):
        self.x = x
        self.y = y
        self.reset()

    def move(self):

        if self.rect.top < margin:
            self.speed_y *= -1
        if self.rect.bottom > screen.get_height():
            self.speed_y *= -1
        if self.rect.colliderect(player_paddle.rect) or self.rect.colliderect(cpu_paddle.rect):
            self.speed_x *= -1

        if self.rect.left < 0:
            self.winner = 1
        if self.rect.right > screen.get_width():
            self.winner = -1

        self.rect.x += self.speed_x
        self.rect.y += self.speed_y

        return self.winner

    def draw(self):
        pygame.draw.circle(screen, white, (self.rect.x + self.ball_rec, self.rect.y + self.ball_rec), self.ball_rec)

    def reset(self):
        self.ball_rec = 8
        self.rect = pygame.Rect(self.x, self.y, self.ball_rec * 2, self.ball_rec * 2)
        self.speed_x = -4
        self.speed_y = 4
        self.winner = 0


# paddles
player_paddle = paddle(screen.get_width() - 40, screen.get_height() // 2)
cpu_paddle = paddle(20, screen.get_height() // 2)
# ball
ball = ball(screen.get_width() - 60, screen.get_height() // 2 + 50)

while True:
    # draw all elements
    draw_board()
    draw_text(f'CPU: {cpu_score}', font, white, 20, 15)
    draw_text(f'P1: {player_score}', font, white, screen.get_width() - 100, 15)
    draw_text(f'Ball Speed: {abs(ball.speed_x)}', font, white, screen.get_width() // 2 - 70, 15)

    # paddles
    player_paddle.draw()
    cpu_paddle.draw()
    if live_ball:
        speed_increase += 1
        winner = ball.move()
        if winner == 0:
            player_paddle.move()
            ball.draw()
            cpu_paddle.ai()
        else:
            live_ball = False
            if winner == 1:
                player_score += 1
            else:
                cpu_score += 1
        player_paddle.draw()

    if live_ball != True:
        if winner == 0:
            draw_text(f'CLICK ANYWHERE TO START', font, white, screen.get_width() // 2 - 150,
                      screen.get_height() // 2 - 50)
        if winner == 1:
            draw_text(f'YOU SCORED!', font, white, screen.get_width() // 2 - 75, screen.get_height() // 2 - 100)
            draw_text(f'CLICK ANYWHERE TO START', font, white, screen.get_width() // 2 - 150,
                      screen.get_height() // 2 - 50)
        if winner == -1:
            draw_text(f'CPU SCORED', font, white, screen.get_width() // 2 - 75, screen.get_height() // 2 - 100)
            draw_text(f'CLICK ANYWHERE TO START', font, white, screen.get_width() // 2 - 150,
                      screen.get_height() // 2 - 50)

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
        if event.type == pygame.MOUSEBUTTONDOWN and live_ball == False:
            live_ball = True
            ball.reset()
            speed_increase = 0

    if speed_increase >= 500:
        speed_increase = 0
        if ball.speed_x < 0:
            ball.speed_x -= 1
        if ball.speed_x > 0:
            ball.speed_x += 1
        if ball.speed_y < 0:
            ball.speed_y -= 1
        if ball.speed_y > 0:
            ball.speed_y += 1
    # update screen
    pygame.display.update()

    clock.tick(60)
